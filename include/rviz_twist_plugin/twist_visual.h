#ifndef TWIST_VISUAL_H
#define TWIST_VISUAL_H

#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Twist.h>
#include <OGRE/OgreVector3.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>

#include <rviz/ogre_helpers/arrow.h>

namespace Ogre
{
  class Vector3;
  class Quaternion;
}

namespace rviz
{
  class Arrow;
  class BillboardLine;
}

namespace rviz_twist_plugin
{

class TwistVisual
{
 public:
  TwistVisual( Ogre::SceneManager* scene_manager, Ogre::SceneNode* parent_node );
  virtual ~TwistVisual();

  // Configure the visual to show the data in the message.
  void setMessage( const geometry_msgs::TwistStamped::ConstPtr& msg );

  // Set the pose of the coordinate frame the message refers to.
  // These could be done inside setMessage(), but that would require
  // calls to FrameManager and error handling inside setMessage(),
  // which doesn't seem as clean.  This way ImuVisual is only
  // responsible for visualization.
  void setFramePosition( const Ogre::Vector3& position );
  void setFrameOrientation( const Ogre::Quaternion& orientation );

  // Set the color and alpha of the visual, which are user-editable
  // parameters and therefore don't come from the Imu message.
  void setColor( float r, float g, float b, float a );
  void setStaticColor_rotate( float r, float g, float b, float a );
  void setRotateColor( double rotation );
  void setLineWidth( float width );
  void setRainbowSwith( bool onoff);

 protected:
  
  // The object implementing the actual arrow shape
  boost::shared_ptr<rviz::Arrow> acceleration_arrow_;
  boost::shared_ptr<rviz::BillboardLine> rotate_circle_;
  boost::shared_ptr<rviz::Arrow> rotate_arrow_;

  // A SceneNode whose pose is set to match the coordinate frame of
  // the Imu message header.
  Ogre::SceneNode* frame_node_;

  // The SceneManager, kept here only so the destructor can ask it to
  // destroy the ``frame_node_``.
  Ogre::SceneManager* scene_manager_;

  float line_width;
  bool rainbow_flag;
};
// END_TUTORIAL

} // namespace

#endif // TWIST_VISUAL_H
