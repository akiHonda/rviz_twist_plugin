#ifndef MESSAGE_FILTER_DISPLAY_HEADER_H
#define MESSAGE_FILTER_DISPLAY_HEADER_H

#include "rviz/display.h"
#include "rviz/message_filter_display.h"


#ifndef Q_MOC_RUN
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>

#include <message_filters/subscriber.h>
#include <tf/message_filter.h>
#include <nav_msgs/Odometry.h>
#endif



namespace rviz
{
template<class MessageType>
  class MessageFilterDisplayWithHeader: public _RosTopicDisplay
  {
    // No Q_OBJECT macro here, moc does not support Q_OBJECT in a templated class.
  public:
    /** @brief Convenience typedef so subclasses don't have to use
     * the long templated class name to refer to their super class. */
    typedef MessageFilterDisplayWithHeader<MessageType> MFDClass;

    MessageFilterDisplayWithHeader()
      : tf_filter_( NULL )
      , displayBaseType( 0 )
      , messages_received_( 0 )
      {
	//QString message_type = QString::fromStdString( ros::message_traits::datatype<MessageType>() );
	QString message_type2 = QString::fromStdString( ros::message_traits::datatype<geometry_msgs::Twist>() ); // TODO
	topic_property_->setMessageType( message_type2 );
	topic_property_->setDescription( message_type2 + " topic to subscribe to." );

	//frame_name_ = "base_link";
      }

    virtual void onInitialize()
    {
      tf_filter_ = new tf::MessageFilter<MessageType>( *context_->getTFClient(),
                                                       fixed_frame_.toStdString(), 10, update_nh_ );

      tf_filter_->connectInput( sub_ );
      tf_filter_->registerCallback( boost::bind( &MessageFilterDisplayWithHeader<MessageType>::incomingMessage, this, _1 ));
      context_->getFrameManager()->registerFilterForTransformStatusCheck( tf_filter_, this );
      
    }

    virtual ~MessageFilterDisplayWithHeader()
      {
	unsubscribe();
	delete tf_filter_;
      }

    virtual void reset()
    {
      Display::reset();
      tf_filter_->clear();
      messages_received_ = 0;
    }

    virtual void setTopic( const QString &topic, const QString &datatype )
    {
      topic_property_->setString( topic );
    }

  protected:
    virtual void updateTopic()
    {
      unsubscribe();
      reset();
      subscribe();
      context_->queueRender();
    }

    virtual void subscribe()
    {
      if( !isEnabled() )
	{
	  return;
	}

      try
	{
	  //sub_.subscribe( update_nh_, topic_property_->getTopicStd(), 10 ); // check 	  // topic_property_->getTopicStd(): Topic name,
	  // TODO
	  //std::cout << "para: " << topic_property_->getTopicStd() << std::endl;
	  sub2 = update_nh_.subscribe(topic_property_->getTopicStd(), 1000, &MessageFilterDisplayWithHeader::twistCallback, this);
	  std::string tpname = "/rviz/tmp/" + topic_property_->getTopicStd();
	  trans_pub = update_nh_.advertise<MessageType>(tpname, 1000);
	  sub_.subscribe( update_nh_, tpname, 10 ); // check
	  //std::cout << "tpname: " << tpname << std::endl;
	  ////
	  setStatus( StatusProperty::Ok, "Topic", "OK" );
	}
      catch( ros::Exception& e )
	{
	  setStatus( StatusProperty::Error, "Topic", QString( "Error subscribing: " ) + e.what() );
	}
    }

    virtual void unsubscribe()
    {
      sub_.unsubscribe();
    }

    virtual void onEnable()
    {
      subscribe();
    }

    virtual void onDisable()
    {
      unsubscribe();
      reset();
    }

    virtual void fixedFrameChanged()
    {
      if (displayBaseType == 0){
	tf_filter_->setTargetFrame( fixed_frame_.toStdString() );
      }else if (displayBaseType == 1){
	// TODO
	tf_filter_->setTargetFrame( frame_name_.toUtf8().constData() );
      }
      reset();
    }

    /** @brief Incoming message callback.  Checks if the message pointer
     * is valid, increments messages_received_, then calls
     * processMessage(). */
    void incomingMessage( const typename MessageType::ConstPtr& msg )
    {
      //std::cout << "incomingMessage" << std::endl;
      if( !msg )
	{
	  return;
	}

      ++messages_received_;
      setStatus( StatusProperty::Ok, "Topic", QString::number( messages_received_ ) + " messages received" );

      processMessage( msg ); // TODO
    }

    /** @brief Implement this to process the contents of a message.
     *
     * This is called by incomingMessage(). */
    virtual void processMessage( const typename MessageType::ConstPtr& msg ) = 0;
    //virtual void processMessage( const typename geometry_msgs::Twist::ConstPtr& msg ) = 0;

    void twistCallback(const geometry_msgs::Twist::ConstPtr& msg)
    {
      //TODO
      if( topic_property_->getTopicStd() != ""){
	if(displayBaseType == 0){
	  // IF TF
	  //std::cout << "twistCallback_tf" << std::endl;
	  msg_trans.header.stamp = ros::Time::now();
	  msg_trans.header.frame_id = frame_name_.toUtf8().constData(); // TODO UTF8?
	  msg_trans.twist.linear = msg->linear;
	  msg_trans.twist.angular = msg->angular;
	  trans_pub.publish(msg_trans);
	  //std::cout << "pub: " << std::endl;
	}else if(displayBaseType==1){
	  // IF odom 
	  //std::cout << "twistCallback_odom" << std::endl;
	  msg_trans.header.stamp = ros::Time::now();
	  msg_trans.header.frame_id = frame_name_.toUtf8().constData(); // TODO UTF8?
	  msg_trans.twist.linear = msg->linear;
	  msg_trans.twist.angular = msg->angular;
	  trans_pub.publish(msg_trans);
	  //TODO
	}
      }
    }

    void updateBodyFrame(QString name, int type){
      if(type==0){
	// If TF
	frame_name_ = name;
	displayBaseType = 0;
      }else if(type==1){
	// If odom
	frame_name_ = name;
	displayBaseType = 1;
	sub_odom.shutdown();
	sub_odom = update_nh_.subscribe(name.toUtf8().constData(), 1000, &MessageFilterDisplayWithHeader::odomCallback, this);
      }
      
    }

    void odomCallback(const nav_msgs::Odometry::ConstPtr& msg){
      odom_position.x = msg->pose.pose.position.x;
      odom_position.y = msg->pose.pose.position.y;
      odom_position.z = msg->pose.pose.position.z;

      odom_orientation.x = msg->pose.pose.orientation.x;
      odom_orientation.y = msg->pose.pose.orientation.y;
      odom_orientation.z = msg->pose.pose.orientation.z;
      odom_orientation.w = msg->pose.pose.orientation.w;
    }

    QString frame_name_;
    message_filters::Subscriber<MessageType> sub_;
    tf::MessageFilter<MessageType>* tf_filter_;
    uint32_t messages_received_;
    ros::Publisher trans_pub;
    MessageType msg_trans;

    ros::Subscriber sub2, sub_odom;
    int displayBaseType;
    Ogre::Quaternion odom_orientation;
    Ogre::Vector3 odom_position;

  };
}

#endif // MESSAGE_FILTER_DISPLAY_HEADER_H 
