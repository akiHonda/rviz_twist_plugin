#ifndef TWIST_DISPLAY_H
#define TWIST_DISPLAY_H

//#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Twist.h>
//#include <rviz/message_filter_display.h>
#include <rviz_twist_plugin/message_filter_display.h>

#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>

#include <tf/transform_listener.h>

#include <rviz/visualization_manager.h>
#include <rviz/properties/color_property.h>
#include <rviz/properties/float_property.h>
#include <rviz/properties/int_property.h>
#include <rviz/properties/string_property.h>
#include <rviz/properties/enum_property.h>
#include <rviz/properties/tf_frame_property.h>
#include <rviz/properties/ros_topic_property.h>
#include <rviz/properties/bool_property.h>
#include <rviz/frame_manager.h>
//#include <rviz_twist_plugin/frame_manager.h>

#include <boost/circular_buffer.hpp>

#include "twist_visual.h"

namespace Ogre
{
  class SceneNode;
}

namespace rviz
{
  class ColorProperty;
  class FloatProperty;
  class IntProperty;
  class StringProperty;
  class EnumProperty;
  class TfFrameProperty;
  class RosTopicProperty;
  class BoolProperty;
}

namespace rviz_twist_plugin
{
  class TwistDisplay: public rviz::MessageFilterDisplayWithHeader<geometry_msgs::TwistStamped>
  {
    Q_OBJECT
  public:
    
    TwistDisplay();
    virtual ~TwistDisplay();
    
  protected:
    virtual void onInitialize();
    
    // A helper to clear this display back to the initial state.
    virtual void reset();
    
    // These Qt slots get connected to signals indicating changes in the user-editable properties.
    private Q_SLOTS:
      void updateColorAndAlpha();
      void updateHistoryLength();
      void updateFrameName();
      void updateLineWidth();
      void updateEnum();
      void updateOdomTopicName();
      void updateDisplayHeight();
      void updateEnableRainbow();
      void updateRainbowLimit();

      // Function to handle an incoming ROS message.
  private:
      void processMessage( const geometry_msgs::TwistStamped::ConstPtr& msg );
      
      // Storage for the list of visuals.  It is a circular buffer where
      // data gets popped from the front (oldest) and pushed to the back (newest)
      boost::circular_buffer<boost::shared_ptr<TwistVisual> > visuals_;
      
      // User-editable property variables.
      rviz::ColorProperty* color_property_;
      rviz::FloatProperty* alpha_property_;
      rviz::IntProperty* history_length_property_;
      //rviz::StringProperty* frame_property_;
      rviz::TfFrameProperty* frame_property_;
      rviz::FloatProperty* line_width_property_;
      rviz::FloatProperty* display_height_property_;
      rviz::EnumProperty* enum_property_;
      rviz::RosTopicProperty* topic_property_;
      rviz::BoolProperty* enable_rainbow_property_;
      rviz::FloatProperty* rainbow_upper_property_;

      double height_offset;
  };
  
}

#endif // TWIST_DISPLAY_H
