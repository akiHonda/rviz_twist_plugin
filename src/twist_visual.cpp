#include <OGRE/OgreVector3.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>

#include <rviz/ogre_helpers/arrow.h>
#include <rviz/ogre_helpers/billboard_line.h>

#include "rviz_twist_plugin/twist_visual.h"

namespace rviz_twist_plugin
{

  TwistVisual::TwistVisual( Ogre::SceneManager* scene_manager, Ogre::SceneNode* parent_node )
  {
    scene_manager_ = scene_manager;

    // Ogre::SceneNode s form a tree, with each node storing the
    // transform (position and orientation) of itself relative to its
    // parent.  Ogre does the math of combining those transforms when it
    // is time to render.
    //
    // Here we create a node to store the pose of the Imu's header frame
    // relative to the RViz fixed frame.
    frame_node_ = parent_node->createChildSceneNode();

    // We create the arrow object within the frame node so that we can
    // set its position and direction relative to its header frame.
    acceleration_arrow_.reset(new rviz::Arrow( scene_manager_, frame_node_ ));
    rotate_arrow_.reset(new rviz::Arrow( scene_manager_, frame_node_ ));
    rotate_circle_.reset(new rviz::BillboardLine( scene_manager_, frame_node_ ));

    line_width = 0.05;
    rainbow_flag = false;
  }

  TwistVisual::~TwistVisual()
  {
    // Destroy the frame node since we don't need it anymore.
    scene_manager_->destroySceneNode( frame_node_ );
  }

  void TwistVisual::setMessage( const geometry_msgs::TwistStamped::ConstPtr& msg )
  {
    const geometry_msgs::Vector3& a = msg->twist.linear;

    // Convert the geometry_msgs::Vector3 to an Ogre::Vector3.
    Ogre::Vector3 acc( a.x, a.y, a.z );

    // Find the magnitude of the acceleration vector.
    float user_scale = 1.0; // TODO
    float length = acc.length() * user_scale * 2.5;

    // Scale the arrow's thickness in each dimension along with its length.
    Ogre::Vector3 scale( length, length, length );
    acceleration_arrow_->setScale( scale );

    // Set the orientation of the arrow to match the direction of the
    // acceleration vector.
    acceleration_arrow_->setDirection( acc );


    /*
    const geometry_msgs::Vector3& a = msg->linear_acceleration;

    // Convert the geometry_msgs::Vector3 to an Ogre::Vector3.
    Ogre::Vector3 acc( a.x, a.y, a.z );

    // Find the magnitude of the acceleration vector.
    float length = acc.length();

    // Scale the arrow's thickness in each dimension along with its length.
    Ogre::Vector3 scale( length, length, length );
    acceleration_arrow_->setScale( scale );

    // Set the orientation of the arrow to match the direction of the
    // acceleration vector.
    acceleration_arrow_->setDirection( acc );
    */

    double rotation = msg->twist.angular.z;
    if(rotation != 0.0) {
      double width_ = line_width;
      double scale_ = 2.0;
      
      (rotation > 0) ? (rotation += 0.1) : (rotation -= 0.1);

      rotate_arrow_->set(0, width_*2*scale_, width_*2*1.0*1.0*scale_, width_*2*0.5*scale_); // set (float shaft_length, float shaft_radius, float head_length, float head_radius)
      if ( rotation > 0 ) {
	rotate_arrow_->setDirection(Ogre::Vector3(-1,0,0));
      } else {
	//rotate_arrow_->setDirection(Ogre::Vector3( 1,0,0));
	rotate_arrow_->setDirection(Ogre::Vector3(-1,0,0));
      }
      rotate_arrow_->setPosition(Ogre::Vector3(0, rotation*scale_, 0));
      rotate_circle_->clear();
      rotate_circle_->setLineWidth(width_);
      if(rainbow_flag){
	setRotateColor(rotation);
      }else{
	if(rotation > 0.0) setStaticColor_rotate(1.0, 0.0, 0.0, 1.0);
	if(rotation < 0.0) setStaticColor_rotate(0.0, 0.0, 1.0, 1.0);
      }
      for (int i = 0; i < 20; i++) {
	Ogre::Vector3 point = Ogre::Vector3((0.0 + rotation*scale_)*sin(i*2*M_PI/32), (0.0 + rotation*scale_)*cos(i*2*M_PI/32), 0);
	if ( rotation < 0 ) point.x = -point.x;
	rotate_circle_->addPoint(point);
      }
    }else{
      rotate_circle_->clear();
      rotate_arrow_->setColor(0,0,0,0);
    }


  }

  // Position and orientation are passed through to the SceneNode.
  void TwistVisual::setFramePosition( const Ogre::Vector3& position )
  {
    /*double height_offset = 0.5; // TODO
    Ogre::Vector3 vec3pos(0.,0.,height_offset);
    vec3pos += position;
    frame_node_->setPosition( vec3pos );*/
    frame_node_->setPosition( position );
  }

  void TwistVisual::setFrameOrientation( const Ogre::Quaternion& orientation )
  {
    frame_node_->setOrientation( orientation );
  }

  // Color is passed through to the Arrow object.
  void TwistVisual::setColor( float r, float g, float b, float a )
  {
    acceleration_arrow_->setColor( r, g, b, a );
  }

  void TwistVisual::setStaticColor_rotate( float r, float g, float b, float a )
  {
    rotate_circle_->setColor( b, g, r, a );
    rotate_arrow_->setColor( b, g, r, a );
  }

  void TwistVisual::setRainbowSwith( bool onoff){
    rainbow_flag = onoff;
  }

  void TwistVisual::setRotateColor( double rotation )
  {
    double step1 = 1.0;
    double step2 = 0.7;
    double step3 = 0.5;
    double r, g, b;

    if(abs(rotation) > step1){
      r=1.0; g=0.0; b=0.0;
    }else if(abs(rotation) > step2){
      double ratio = (rotation-step2) / (step1-step2);
      r = ratio;
      g = 1.0 - ratio;
      b = 0.0;
    }else if(abs(rotation) > step3){
      double ratio = (rotation-step3) / (step2-step3);
      r = 0.0;
      g = ratio;
      b = 1.0-ratio;
    }else{
      r=0.0; g=0.0; b=1.0;
    }

    rotate_circle_->setColor( r, g, b, 1 );
    rotate_arrow_->setColor( r, g, b, 1 );
  }

  void TwistVisual::setLineWidth( float width )
  {
    line_width = width;
  }
} // end namespace 
