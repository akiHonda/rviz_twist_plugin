#include "rviz_twist_plugin/twist_display.h"
#include <rviz/display_context.h>

namespace rviz_twist_plugin
{
  TwistDisplay::TwistDisplay()
  {
  }
  
  void TwistDisplay::onInitialize()
  {
    MFDClass::onInitialize();

    // Overwrite
    //topic_property_->setName( QString::fromStdString("Twist Topic") );
    //topic_property_->setValue("cmd_vel");

    enum_property_ = new rviz::EnumProperty( "Display base", "TF",
					       "Select base position where marker will display",
					     this, SLOT( updateEnum() ));
    enum_property_->addOption("TF", 0);
    enum_property_->addOption("Odometry", 1);
    
    /*frame_property_ = new rviz::StringProperty( "Target TF Frame", "base_link",
						"Name of body which has a twist msg.",
						this, SLOT( updateFrameName() ));*/

    frame_property_ = new rviz::TfFrameProperty( "Target TF Frame", "<Fixed Frame>",
						"Name of body which has a twist msg.",
						this, context_->getFrameManager(), false );
    updateBodyFrame(frame_property_->getString(), 0);

    topic_property_ = new rviz::RosTopicProperty( "Odom Topic", "/odom",  QString::fromStdString( ros::message_traits::datatype<nav_msgs::Odometry>() ),
						 "Name of body which has a twist msg.",
						  this, SLOT( updateOdomTopicName() ));
    // Choose TF or Odom
    if( enum_property_->getOptionInt() == 0){
      frame_property_->setHidden(false);
      topic_property_->setHidden(true);
    }else if(enum_property_->getOptionInt() == 1){
      frame_property_->setHidden(true);
      topic_property_->setHidden(false);
    }

    display_height_property_ = new rviz::FloatProperty( "Display height", 0.5,
					       "Marker height",
					       this, SLOT( updateDisplayHeight() ));
    updateDisplayHeight();

    color_property_ = new rviz::ColorProperty( "Color", QColor( 204, 51, 204 ),
					       "Color to draw the acceleration arrows.",
					       this, SLOT( updateColorAndAlpha() ));
    
    alpha_property_ = new rviz::FloatProperty( "Alpha", 1.0,
					       "0 is fully transparent, 1.0 is fully opaque.",
					       this, SLOT( updateColorAndAlpha() ));

    enable_rainbow_property_ = new rviz::BoolProperty( "Rainbow Color", false,
						       "Enable rainbow colour",
						       this, SLOT( updateEnableRainbow() ));

    rainbow_upper_property_ = new rviz::FloatProperty( "Rainbow Upper value", 1.0,
						       "Max value of rainbow colour",
						       this, SLOT( updateRainbowLimit() ));
    if( enable_rainbow_property_->getValue() == false ) rainbow_upper_property_->setHidden(true);


    history_length_property_ = new rviz::IntProperty( "History Length", 1,
						      "Number of prior measurements to display.",
						      this, SLOT( updateHistoryLength() ));

    line_width_property_ = new rviz::FloatProperty( "Line Width", 0.05,
					       "Line width of markers. Change is appried at next marker update",
					       this, SLOT( updateLineWidth() ));

    history_length_property_->setMin( 1 );
    history_length_property_->setMax( 100000 );

    // TODO
    color_property_->setHidden(true);
    //alpha_property_->setHidden(true);
    history_length_property_->setHidden(true);
    line_width_property_->setHidden(true);
    
    updateHistoryLength();
  }
  
  TwistDisplay::~TwistDisplay()
  {
  }
  
  // Clear the visuals by deleting their objects.
  void TwistDisplay::reset()
  {
    MFDClass::reset();
    visuals_.clear();
  }
  
  void TwistDisplay::updateEnum(){
    // Choose TF or Odom
    if( enum_property_->getOptionInt() == 0){
      frame_property_->setHidden(false);
      topic_property_->setHidden(true);
      displayBaseType = 0;
    }else if(enum_property_->getOptionInt() == 1){
      frame_property_->setHidden(true);
      topic_property_->setHidden(false);
      displayBaseType = 1;
      updateBodyFrame(topic_property_->getString(), 1);
      //std::cout << "updateEnum_odom" << std::endl;
    }
    

  }

  void TwistDisplay::updateDisplayHeight(){
    height_offset = display_height_property_->getFloat();
  }

  // Set the current color and alpha values for each visual.
  void TwistDisplay::updateColorAndAlpha()
  {
    float alpha = alpha_property_->getFloat();
    Ogre::ColourValue color = color_property_->getOgreColor();
    
    for( size_t i = 0; i < visuals_.size(); i++ )
      {
	visuals_[ i ]->setColor( color.r, color.g, color.b, alpha );
	visuals_[ i ]->setStaticColor_rotate( color.r, color.g, color.b, alpha );
      }
  }

  void TwistDisplay::updateEnableRainbow(){
    bool temp_flag;
    if( enable_rainbow_property_->getValue() == false ){
      rainbow_upper_property_->setHidden(true);
      temp_flag = false;
    }
    if( enable_rainbow_property_->getValue() == true ){
      rainbow_upper_property_->setHidden(false);
      temp_flag = true;
    }

    for( size_t i = 0; i < visuals_.size(); i++ )
      {
        visuals_[ i ]->setRainbowSwith( temp_flag );
      }

  }

  void TwistDisplay::updateRainbowLimit(){

  }

  // Set the number of past visuals to show.
  void TwistDisplay::updateHistoryLength()
  {
    visuals_.rset_capacity(history_length_property_->getInt());
  }

  void TwistDisplay::updateFrameName()
  {
    updateBodyFrame(frame_property_->getString(), 0);
  }

  void TwistDisplay::updateOdomTopicName(){
    updateBodyFrame(topic_property_->getString(), 1);
  }

  void TwistDisplay::updateLineWidth()
  {
    for( size_t i = 0; i < visuals_.size(); i++ ){
      visuals_[i]->setLineWidth( line_width_property_->getFloat() );
    }
  }

  // This is our callback to handle an incoming message.
  void TwistDisplay::processMessage( const geometry_msgs::TwistStamped::ConstPtr& msg )
  {
    
    // Here we call the rviz::FrameManager to get the transform from the
    // fixed frame to the frame in the header of this Imu message.  If
    // it fails, we can't do anything else so we return.
    Ogre::Quaternion orientation;
    Ogre::Vector3 position;
    
    if( displayBaseType == 0){
      // IF TF mode
      if( !context_->getFrameManager()->getTransform( msg->header.frame_id,
						      msg->header.stamp,
						      position, orientation ))
	{
	  ROS_DEBUG( "Error transforming from frame '%s' to frame '%s'",
		     msg->header.frame_id.c_str(), qPrintable( fixed_frame_ ));
	  return;
	}
      position.z += height_offset;

    }else if( displayBaseType == 1){
      // IF Odom mode
      //std::cout << "processMessage_odom" << std::endl;
      position.x = odom_position.x;
      position.y = odom_position.y;
      position.z = odom_position.z + height_offset;
      orientation = odom_orientation;
    }

    // We are keeping a circular buffer of visual pointers.  This gets
    // the next one, or creates and stores it if the buffer is not full
    boost::shared_ptr<TwistVisual> visual;
    if( visuals_.full() )
      {
	visual = visuals_.front();
      }
    else
      {
	visual.reset(new TwistVisual( context_->getSceneManager(), scene_node_ ));
      }

    // Now set or update the contents of the chosen visual.
    visual->setMessage( msg );
    visual->setFramePosition( position );
    visual->setFrameOrientation( orientation );

    float alpha = alpha_property_->getFloat();
    Ogre::ColourValue color = color_property_->getOgreColor();
    visual->setColor( color.r, color.g, color.b, alpha );

    // And send it to the end of the circular buffer
    visuals_.push_back(visual);
    
  }

} // namespace

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(rviz_twist_plugin::TwistDisplay,rviz::Display )
