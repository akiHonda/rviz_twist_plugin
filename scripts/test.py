#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from geometry_msgs.msg import TwistStamped

# Publish TwistWithCovarianceStamped msg based on Twist msg

pub = rospy.Publisher('/cmd_vel_stamped', TwistStamped, queue_size=10)
pub2 = rospy.Publisher('/cmd_vel', Twist, queue_size=10)

def listener():
    rospy.init_node('cmdVel_republisher', anonymous=True)

    r = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        msg = TwistStamped()
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = "map"
        msg.twist.linear.x = 1.0
        pub.publish(msg)

        msg2 = Twist()
        msg2.linear.x = 1.0
        pub2.publish(msg2)

        r.sleep()
        
if __name__ == '__main__':
    try:
        listener()
    except rospy.ROSInterruptException: pass
