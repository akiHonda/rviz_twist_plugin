rviz_twist_plugin
======================
RViz plugin to display geometry_msgs/Twist type message.  
Ver0.6

![twist_plugin.png](https://bitbucket.org/repo/MR6aAK/images/4097761637-twist_plugin.png)

Usage
------
* git clone  
* catkin_make

Parameters
----------------


Information
--------
N/A

Licences ライセンス
----------
N/A
Copyright &copy; 2011 xxxxxx
Licensed under the [Apache License, Version 2.0][Apache]
Distributed under the [MIT License][mit].
Dual licensed under the [MIT license][MIT] and [GPL license][GPL].

[Apache]: http://www.apache.org/licenses/LICENSE-2.0
[MIT]: http://www.opensource.org/licenses/mit-license.php
[GPL]: http://www.gnu.org/licenses/gpl.html